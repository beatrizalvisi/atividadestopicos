import { Injectable } from '@angular/core';
import { AngularFireAuth } from '@angular/fire/auth';
import { NavController, ToastController } from '@ionic/angular';
import { Observable } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class LoginService {
  isLoggedIn: Observable<User>;

  constructor(
    private nav: NavController,
    private auth: AngularFireAuth,
    private toast: ToastController
  ) {
      this.isLoggedIn = this.auth.authState;
  }

  login(user) {

    this.auth.signInWithEmailAndPassword(user.email, user.password).
    then(() => this.nav.navigateForward('home')). // redireciona o usuário para a home
    catch (() => this.showError()); 
  }

  private async showError() {
    const ctrl = await this.toast.create({ //await permite usar o resultado assincronamente.
      message: 'O dados informados estão incorretos.',
      duration: 4000
    }); 

    ctrl.present();
  }

  createUser(user){
    this.auth.createUserWithEmailAndPassword(user.email, user.password). //Depois que esse metódo for executado, vai para o then.
    then(credentials => console.log(credentials)); //Recebe as credenciais.
  }

  recoverPass(data) {
    this.auth.sendPasswordResetEmail(data.email).
    then(() => this.nav.navigateBack('auth')).
    catch(err => {
      console.log(err);
    });
  }

  logout(){
    this.auth.signOut().
    then(() => this.nav.navigateBack('auth')); // Função de call back.
  }

}
