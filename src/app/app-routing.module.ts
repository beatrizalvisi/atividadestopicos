import { NgModule } from '@angular/core';
import { PreloadAllModules, RouterModule, Routes } from '@angular/router';

const routes: Routes = [
  {
    path: '',
    redirectTo: 'auth',
    pathMatch: 'full'
  },

  {
    path: 'home',
    loadChildren: () => import('./pages/home/home.module').then( m => m.HomePageModule)
  },

  {
    path: 'contas',
    loadChildren: 'src/app/pages/contas/contas.module#ContasModule'
  },

  {
    path: 'auth',
    loadChildren: 'src/app/pages/auth/auth.module#AuthModule'
  },
  {
    path: 'cadastro',
    loadChildren: () => import('./pages/contas/cadastro/cadastro.module').then( m => m.CadastroPageModule)
  },
  {
    path: 'relatorio',
    loadChildren: () => import('./pages/contas/relatorio/relatorio.module').then( m => m.RelatorioPageModule)
  }
];

@NgModule({
  imports: [
    RouterModule.forRoot(routes, { preloadingStrategy: PreloadAllModules })
  ],
  exports: [RouterModule]
})
export class AppRoutingModule { }
