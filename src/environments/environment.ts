// This file can be replaced during build by using the `fileReplacements` array.
// `ng build --prod` replaces `environment.ts` with `environment.prod.ts`.
// The list of file replacements can be found in `angular.json`.

export const environment = {
  production: false, 
  firebaseConfig: {
    apiKey: 'AIzaSyCl8rdRoA30aVCCgzUbSbVNx333wu3dUMA',
    authDomain: 'controle-ifbeatriz.firebaseapp.com',
    databaseURL: 'https://controle-ifbeatriz.firebaseio.com',
    projectId: 'controle-ifbeatriz',
    storageBucket: 'controle-ifbeatriz.appspot.com',
    messagingSenderId: '496354251112',
    appId: '1:496354251112:web:68ac89ae7c816ab82046dd',
    measurementId: 'G-5V4KHNM596'
  }
};

/*
 * For easier debugging in development mode, you can import the following file
 * to ignore zone related error stack frames such as `zone.run`, `zoneDelegate.invokeTask`.
 *
 * This import should be commented out in production mode because it will have a negative impact
 * on performance if an error is thrown.
 */
// import 'zone.js/dist/zone-error';  // Included with Angular CLI.
